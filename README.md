# Create Node
Create a generic project template for node-js server in one command.

# Usage
Install the package as global - 
`npm install -g create-nodejs`

Create the server template - 
`create-nodejs <My Project Name>`

You will be asked to fill some information about the project, you can skip it if you want.
Follow the instructions and you have a new,light-weight and generic template for node-js server.

# About The Template
The template is very simple and nice. It has the minimum requirements for standard node server.
You as developer has the freedom to change or add things as you want, we just give you a nice start.
We will install for you the necessary dependencies for the server, you can ask for more during the process.

The project structure looks like this:
![alt text](https://gitlab.com/ItamarSmirra/create-node/-/raw/master/project-structure.png)