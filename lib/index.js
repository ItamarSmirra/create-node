const colors = require('colors');
const npmUtils = require('./utils/npmUtils');
const nodeUtils = require('./utils/nodeUtils/nodeUtils');
const filesUtils = require('./utils/filesUtils');
const { getInput, getAnswer } = require('./utils/inputUtils');

const createFiles = (appName) => {
    const path = `./${appName}`;
    const packageJson = npmUtils.createPackageJson(appName);
    filesUtils.createFileInProject(`${path}/package.json`, npmUtils.convertJsonToString(packageJson));
    filesUtils.createFileInProject(`${path}/index.js`, nodeUtils.indexJsFile);

    const wantedPort = getInput(`PORT (${nodeUtils.defaultPort}) -> `, nodeUtils.defaultPort);
    filesUtils.createFileInProject(`${path}/\.env`, nodeUtils.dotenvFile(wantedPort));
    
    filesUtils.createFolderInProject(`${path}/src`);
    filesUtils.createFolderInProject(`${path}/src/config`);
    filesUtils.createFileInProject(`${path}/src/config/config.js`, nodeUtils.configJsFile);
    filesUtils.createFolderInProject(`${path}/src/handlers`);
    filesUtils.createFolderInProject(`${path}/src/routes`);
    filesUtils.createFileInProject(`${path}/src/routes/router.js`, nodeUtils.routesFile);
}

const createNodeApp = () => {
    const appName = process.argv[2];
    const path = `./${appName}`;

    if (filesUtils.isFolderExists(path)) {
        console.log(`there is already directory with the name '${appName}'`.brightRed);
        console.log('try different name');
    } else {
        filesUtils.createFolderInProject(path);
        createFiles(appName);

        console.log('\n---- Install Dependencies ----');
        console.log("we will install for you 'express', 'dotenv' and 'cors' dependencies.");
        let extraDependencies = [];

        if (getAnswer('Do You want to add something?')) {
            extraDependencies = getInput('Enter your wanted dependencies seperated by a comma (colors, ramda, moment...) => ');
            extraDependencies = extraDependencies.split(' ').join('').split(',');
        }

        console.log('\nthis might take few seconds...');
        npmUtils.installDependencies(path, [...npmUtils.baseDependencies, ...extraDependencies]);
        console.log('\n Completed!'.green);
        console.log(`now just run -`);
        console.log(`\tcd ${appName}`);
        console.log('\tnpm start');
    }
}

module.exports = createNodeApp;