const fs = require('fs');

const createFileInProject = (filePath, fileData) => fs.writeFileSync(`${filePath}`, fileData);
const createFolderInProject = (folderPath) => fs.mkdirSync(`${folderPath}`);
const isFolderExists = (folderPath) => fs.existsSync(`${folderPath}`);

module.exports = {
    createFileInProject,
    createFolderInProject,
    isFolderExists
}