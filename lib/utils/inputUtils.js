const readLineSync = require('readline-sync');

const getInput = (question, defaultInput = '') => readLineSync.question(question, { defaultInput: defaultInput });
const getAnswer = (question) => readLineSync.keyInYN(question);

module.exports = {
    getInput,
    getAnswer
}