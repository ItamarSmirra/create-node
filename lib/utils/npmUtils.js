const fs = require('fs');
const colors = require('colors');
const { execSync } = require('child_process');
const packageJsonFields = require('../constants/packageJsonFileds');
const { getInput } = require('./inputUtils');
const { pick } = require('./ramdaUtils');

const baseDependencies = ['express', 'dotenv', 'cors'];

createPackageJson = (appName) => {
    const packageJson = JSON.parse(fs.readFileSync(`${__dirname}/../../package.json`));
    const newPackageJson = pick([packageJsonFields.SCRIPTS], packageJson);
    newPackageJson[packageJsonFields.NAME] = appName;
    newPackageJson[packageJsonFields.VERSION] = '1.0.0';
    newPackageJson[packageJsonFields.AUTHOR] = getInput('Author? -> ').replace(/,/g, ' ');
    newPackageJson[packageJsonFields.DESCRIPTION] = getInput('Description? -> ').replace(/,/g, ' ');
    newPackageJson[packageJsonFields.LICENS] = 'ISC';
    newPackageJson[packageJsonFields.REPOSITORY] = {
        "type": "git",
        "url": ''
    }

    return newPackageJson;
}

const convertJsonToString = (packageJson) => (
    JSON.stringify(packageJson).split(',').join(',\n').split('{').join('{\n').split('}').join('}\n')
)

const installDependency = (path, dependency) => {
    if (dependency) {
        console.log(`installing ` + colors.blue(dependency) + `...`);
        execSync(`cd ${path} && npm i ${dependency} --loglevel=error`,
                 { stdio: 'inherit' });
    }
}
const installDependencies = (path, dependencies) => dependencies.forEach(curr => installDependency(path, curr))

module.exports = {
    baseDependencies,
    createPackageJson,
    convertJsonToString,
    installDependencies
}