const fs = require('fs');

const defaultPort = 3000;
const indexJsFile = fs.readFileSync(`${__dirname}/nodeFiles/index.js`);
const configJsFile = fs.readFileSync(`${__dirname}/nodeFiles/config.js`);
const routesFile = fs.readFileSync(`${__dirname}/nodeFiles/routes.js`);
const dotenvFile = (port) => `PORT=${port}`;

module.exports = {
    defaultPort,
    indexJsFile,
    configJsFile,
    dotenvFile,
    routesFile 
}