module.exports = Object.freeze({
    NAME: 'name',
    AUTHOR: 'author',
    VERSION: 'version',
    SCRIPTS: 'scripts',
    DESCRIPTION: 'description',
    LICENS: "license",
    REPOSITORY: "repository"
})